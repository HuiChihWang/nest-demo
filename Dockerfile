FROM node:18-alpine

RUN npm install -g @nestjs/cli

WORKDIR /app
COPY . .

RUN npm install --production

RUN npm run build

CMD [ "npm", "run", "start:prod" ]

EXPOSE 3000