import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { ValidationPipeOptions } from '@nestjs/common/pipes/validation.pipe';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const validationConfig = {
    whitelist: true,
    forbidNonWhitelisted: true,
  } satisfies ValidationPipeOptions;

  app.useGlobalPipes(new ValidationPipe(validationConfig));
  await app.listen(3000);
}
bootstrap();
