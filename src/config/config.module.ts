import { Global, Module } from '@nestjs/common';
import { CacheConfigService } from './cache-config.service';

@Global()
@Module({
  providers: [CacheConfigService],
  exports: [CacheConfigService],
})
export class ConfigModule {}
