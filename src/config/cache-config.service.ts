import { Injectable } from '@nestjs/common';

@Injectable()
export class CacheConfigService {
  getRedisHost(): string {
    return 'localhost';
  }

  getRedisPort(): number {
    return 6379;
  }
}
