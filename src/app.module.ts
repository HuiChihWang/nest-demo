import { Module } from '@nestjs/common';
import { PostsModule } from './posts/posts.module';
import { CacheModule } from '@nestjs/cache-manager';
import { redisStore } from 'cache-manager-redis-yet';
import { ConfigModule } from './config/config.module';
import { CacheConfigService } from './config/cache-config.service';

@Module({
  imports: [
    ConfigModule,
    PostsModule,
    CacheModule.registerAsync({
      isGlobal: true,
      imports: [ConfigModule],
      inject: [CacheConfigService],
      useFactory: async (cacheConfig: CacheConfigService) => ({
        store: redisStore,
        host: cacheConfig.getRedisHost(),
        port: cacheConfig.getRedisPort(),
        max: 100,
      }),
    }),
    ConfigModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
