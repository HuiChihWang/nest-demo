import { Module } from '@nestjs/common';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';
import { PostsRepository } from './posts.repository';
import { ImageService } from './image.service';
import { HttpModule } from '@nestjs/axios';
import { BullModule } from '@nestjs/bull';
import { PostsJobHandler } from './posts.job';
import { ScheduleModule } from '@nestjs/schedule';
import { UploadQueue } from './upload.queue';
import { QueueConstant } from './queue.constant';
import { CacheConfigService } from '../config/cache-config.service';
import { ConfigModule } from '../config/config.module';

@Module({
  imports: [
    HttpModule,
    ScheduleModule.forRoot(),
    BullModule.forRootAsync({
      imports: [ConfigModule],
      inject: [CacheConfigService],
      useFactory: async (cacheConfig: CacheConfigService) => ({
        redis: {
          host: cacheConfig.getRedisHost(),
          port: cacheConfig.getRedisPort(),
        },
      }),
    }),
    BullModule.registerQueue({
      name: QueueConstant.UPLOAD_QUEUE,
    }),
  ],
  providers: [
    PostsService,
    PostsRepository,
    ImageService,
    PostsJobHandler,
    UploadQueue,
  ],
  controllers: [PostsController],
})
export class PostsModule {}
