export enum QueueConstant {
  UPLOAD_QUEUE = 'upload',
}

export enum JobConstant {
  UPLOAD_IMAGE_JOB = 'uploading-image',
}
