import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import { Logger } from '@nestjs/common';
import { UploadStatus } from './posts.enum';
import { PostsService } from './posts.service';
import { ImageService } from './image.service';
import { JobConstant, QueueConstant } from './queue.constant';

@Processor(QueueConstant.UPLOAD_QUEUE)
export class UploadQueue {
  constructor(
    private readonly imageService: ImageService,
    private readonly postService: PostsService,
  ) {}
  @Process(JobConstant.UPLOAD_IMAGE_JOB)
  async uploadImage(job: Job<string>) {
    const postId = job.data;
    Logger.log(`consume job ${job.id} with post ${postId}`);
    const post = await this.postService.getPost(postId);

    if (!post || post.status !== UploadStatus.UPLOADING) {
      return;
    }

    try {
      const { uploadedLink } = await this.imageService.uploadImage(
        post.coverUrl,
      );

      await this.postService.updatePost({
        ...post,
        imgurCoverUrl: uploadedLink,
        status: UploadStatus.DONE,
      });
    } catch (error) {
      console.log(error);
      await this.postService.updatePost({
        ...post,
        status: UploadStatus.ERROR,
      });
    }
  }
}
