import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { Post as PostEntity } from './posts.entity';
import { PostsService } from './posts.service';
import { PostCreateRequest } from './posts.dto';
import { CacheInterceptor, CacheTTL } from '@nestjs/cache-manager';

@Controller('posts')
@UseInterceptors(CacheInterceptor)
export class PostsController {
  constructor(private readonly postService: PostsService) {}

  @Get()
  @CacheTTL(10000)
  async getPosts(): Promise<PostEntity[]> {
    return this.postService.getPosts();
  }

  @Get('/:id')
  @CacheTTL(2000)
  async getPost(@Param('id') id: string) {
    return this.postService.getPost(id);
  }

  @Post()
  async createPost(@Body() req: PostCreateRequest): Promise<PostEntity> {
    return this.postService.createPost(req);
  }
}
