export enum UploadStatus {
  IDLE = 'IDLE',
  UPLOADING = 'UPLOADING',
  DONE = 'DONE',
  ERROR = 'ERROR',
}
