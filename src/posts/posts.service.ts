import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { PostsRepository } from './posts.repository';
import { PostCreateRequest } from './posts.dto';
import { Post } from './posts.entity';
import { UploadStatus } from './posts.enum';

@Injectable()
export class PostsService {
  constructor(private readonly postsRepo: PostsRepository) {}

  async getPosts() {
    Logger.log('fetch posts');
    return this.postsRepo.getAll();
  }

  async getPost(id: string) {
    Logger.log(`get post ${id}`);
    const post = await this.postsRepo.get(id);
    if (!post) {
      throw new BadRequestException(`post ${id} is not found`);
    }

    return post;
  }

  async createPost(createBody: PostCreateRequest) {
    const newPost = await this.postsRepo.save({
      ...createBody,
      status: UploadStatus.IDLE,
    });

    Logger.log(`create post ${JSON.stringify(newPost)}`);
    return newPost;
  }

  async updatePost(post: Post) {
    const isPostExisted = await this.postsRepo.exists(post.id);
    if (!isPostExisted) {
      throw new BadRequestException(`post ${post.id} does not existed`);
    }

    return this.postsRepo.save(post);
  }
}
