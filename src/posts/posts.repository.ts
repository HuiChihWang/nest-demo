import { Injectable } from '@nestjs/common';
import { JsonDB, Config } from 'node-json-db';
import { Post } from './posts.entity';
import { v4 as uuid } from 'uuid';

@Injectable()
export class PostsRepository {
  private static JSON_PATH = '/posts';
  private db: JsonDB;

  constructor() {
    const config = new Config('db', true, true, '/', true);
    this.db = new JsonDB(config);
  }
  async getAll() {
    const posts = await this.db.getObjectDefault(PostsRepository.JSON_PATH, {});
    return Object.values(posts) as [Post];
  }

  async get(id: string) {
    return this.db.getObjectDefault<Post>(this.getObjectKey(id), null);
  }

  async save(data: Partial<Post>) {
    if (!data?.id) {
      data.id = uuid();
    }
    await this.db.push(this.getObjectKey(data.id), data);
    return data as Post;
  }

  async exists(id: string) {
    const post = await this.get(id);
    return !!post;
  }

  private getObjectKey(id: string) {
    return PostsRepository.JSON_PATH + '/' + id;
  }
}
