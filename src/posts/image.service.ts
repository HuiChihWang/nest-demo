import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class ImageService {
  private static readonly IMAGE_API_ENDPOINT = 'https://api.imgur.com/3/image';
  private static readonly HEADER = {
    Authorization: `Client-ID d6648ec43ad3a34`,
  };

  constructor(private readonly httpService: HttpService) {}

  async uploadImage(srcUrl: string) {
    const task$ = this.httpService.post(
      ImageService.IMAGE_API_ENDPOINT,
      {
        image: srcUrl,
      },
      {
        headers: ImageService.HEADER,
      },
    );

    const res = await lastValueFrom(task$);
    return {
      uploadedLink: res?.data?.data?.link,
    };
  }
}
