import { IsUrl } from 'class-validator';

export class PostCreateRequest {
  @IsUrl()
  readonly coverUrl: string;
}
