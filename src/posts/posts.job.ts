import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PostsService } from './posts.service';
import { UploadStatus } from './posts.enum';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { JobConstant, QueueConstant } from './queue.constant';

@Injectable()
export class PostsJobHandler {
  constructor(
    private readonly postService: PostsService,
    @InjectQueue(QueueConstant.UPLOAD_QUEUE) private uploadQueue: Queue<string>,
  ) {}

  @Cron(CronExpression.EVERY_MINUTE)
  async enqueueIdlePosts() {
    const posts = await this.postService.getPosts();
    const idlePosts = posts.filter((post) => post.status === UploadStatus.IDLE);
    for (const idlePost of idlePosts) {
      await this.postService.updatePost({
        ...idlePost,
        status: UploadStatus.UPLOADING,
      });

      const job = await this.uploadQueue.add(
        JobConstant.UPLOAD_IMAGE_JOB,
        idlePost.id,
      );
      Logger.log(`enqueue job ${job.id} with post id ${idlePost.id}`);
    }
  }
}
