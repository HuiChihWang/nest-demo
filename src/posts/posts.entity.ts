import { UploadStatus } from './posts.enum';

export class Post {
  id: string;
  coverUrl: string;
  imgurCoverUrl?: string;
  status: UploadStatus;
}
